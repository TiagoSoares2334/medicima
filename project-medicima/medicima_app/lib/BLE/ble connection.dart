import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';

class BluetoothApp extends StatelessWidget {
  const BluetoothApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      home: MyHomePage(title: 'XPENSES'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class Device {
  String name = "";

  Device(this.name);
}

class _MyHomePageState extends State<MyHomePage> {
  List<Device> devices = [];
  Future<void> _changeDevices(String name) async {
    // ignore: await_only_futures
    //devices.add(name);
   // await devices.add(name);

  } 

  @override
  Widget build(BuildContext context) {
    
    FlutterBlue flutterBlue = FlutterBlue.instance;

    flutterBlue.startScan(timeout: const Duration(seconds: 20));

    var subs = flutterBlue.scanResults.listen((results) {
      for (ScanResult s in results) {
        Device d = Device(s.device.name);
        _changeDevices(s.device.name);
        //print('${s.device.name} found! ${s.rssi}');
        print('${s}');
      }
    });
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            ListView.separated(
              shrinkWrap: true,
              itemCount: devices.length,
              separatorBuilder: (BuildContext context, int index) {
                return const SizedBox(height: 10);
              },
              itemBuilder: (context, index) {
                return Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Text(devices[index].name),
                  ],
                );
              },
            ),
          ]
        ),
      ),
    );
  }
}
