// ignore_for_file: avoid_unnecessary_containers

import 'package:flutter/material.dart';
import 'package:flutter_blue/flutter_blue.dart';
import 'package:medicima_app/BLE/ble%20connection.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const MaterialApp(
      title: 'Xpenses',
      home: MyHomePage(title: 'XPENSES'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class Device {
  String name = "";

  Device(this.name);
}

class _MyHomePageState extends State<MyHomePage> {
  List<Device> devices = [];
  Future<void> _changeDevices(String name) async {
    // ignore: await_only_futures
    //devices.add(name);
    // await devices.add(name);
  }

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.height;

    double spentToday = 120;

    return Scaffold(
      appBar: AppBar(
        title: Padding(
            padding: const EdgeInsets.only(left: 10),
            child: Text(widget.title,
                style: const TextStyle(color: Colors.black))),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10),
            child: TextButton(onPressed: () {}, child: const Text("Account")),
          ),
        ],
        backgroundColor: Colors.white,
        elevation: 0.0,
        centerTitle: false,
      ),
      body: Center(
        child: Column(
          children: [
            SingleChildScrollView(
              scrollDirection: Axis.horizontal,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Container(
                    width: width * .5,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: const Color(0xff2a9d8f),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.only(left: 0, right: 0),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              const Text(
                                "Today",
                              ),
                              IconButton(
                                onPressed: () {},
                                icon: const Icon(Icons.arrow_forward_ios),
                                iconSize: 10,
                              )
                            ],
                          ),
                        ),
                        Text(
                          "Total spent $spentToday \$00",
                        ),
                      ],
                    ),
                  ),
                  Container(
                    width: width * .5,
                    padding: const EdgeInsets.all(10),
                    decoration: BoxDecoration(
                      color: const Color(0xfff4a261),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            const Text("Due for today"),
                            IconButton(
                              onPressed: () {},
                              icon: const Icon(Icons.arrow_forward_ios),
                              iconSize: 10,
                            )
                          ],
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Center(
                child: ElevatedButton(
                    onPressed: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => const BluetoothApp(),
                        ),
                      );
                    },
                    child: const Text("Connect to Bluetooth Device")))
          ],
        ),
      ),
    );
  }
}
